function autoRefresh(event) {
    fetch(window.location + "/ping")
        .then((response) => response.text())
        .then((response) => {
            if (response === "Download") {
                let link = document.createElement("a");
                link.download = "Dataset";
                link.href = window.location + "/download/train";
                link.click();
                link.href = window.location + "/download/test";
                link.click();
                document.querySelector("#done").click();
            } else if (response === "Wait") {
                window.setTimeout(autoRefresh, 2000);
            }
        });
}

document.addEventListener("DOMContentLoaded", function(event) {
    window.setTimeout(autoRefresh, 2000);
});
