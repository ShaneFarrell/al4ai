function autoSubmit(event) {
    if (document.querySelector("#enable-auto-submit").checked) {
        document.querySelector("#submit-button").click();
    }
}

document.addEventListener("DOMContentLoaded", function(event) {
    document.querySelectorAll(".auto-submit").forEach(node =>
        node.addEventListener("change", autoSubmit)
    );
});
