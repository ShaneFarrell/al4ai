from django.views.decorators.http import require_http_methods

from django.shortcuts import HttpResponse
import json
from worker.task_scheduler import task_scheduler
from worker.run import run_active_learning, run_predict_dataset
from worker.settings import CACHE_SIZE
from . import cache
from .task_scheduler import thread_storage


@require_http_methods(["POST"])
def run_view(request):
    schedule_task_from_request("active_learning", run_active_learning, request)
    return HttpResponse()


@require_http_methods(["POST"])
def dataset_view(request):
    schedule_task_from_request("predict_dataset", run_predict_dataset, request)
    return HttpResponse()


def schedule_task_from_request(task_type, task_function, request):
    kwargs = json.loads(request.body)

    settings: dict = kwargs["settings"]
    labels: dict = kwargs["labels"]
    project_id: str = kwargs["id"]

    task_scheduler.schedule_task(
        project_id, task_type, task_function, settings=settings, label_map=labels
    )


def home_view(request):
    return HttpResponse(f"""
    AL4AI Worker running.<br/><br/>
    <b>Cache size: </b>{cache.current_cache_size / (1024 ** 2):.2f} MB / {CACHE_SIZE / (1024 ** 3):.1f} GB<br/><br/>
    <b>Thread status:</b><br/>
    <ol>
    <li>{'</li><li>'.join(thread_storage["status"].values())}</li>
    </ol>
    """)
