from worker.dataset import get_dataset
from worker.models.machine_learning_model import get_machine_learning_model
from worker.models.representation import get_representation
from worker.models.evaluation import evaluate_model
from random import choice
import numpy as np
from worker.task_scheduler import send_stats, send_results


def run_predict_dataset(settings, label_map, *, train_dataset=None, test_dataset=None):
    if settings.get("DatasetLocation"):
        train_dataset = get_dataset(settings["DatasetLocation"])

    # Load the training dataset
    unlabelled_urls = train_dataset.get_unlabelled_urls(label_map)
    labelled_urls = train_dataset.get_labelled_urls(label_map)
    labels = train_dataset.get_labels(label_map)

    unique_labels = list(set(labels))

    # Load the test dataset if there is one
    test_urls = []
    if settings.get("TestDatasetLocation"):
        test_dataset = get_dataset(settings["TestDatasetLocation"])

    if test_dataset is not None:
        test_urls = test_dataset.get_all_urls()

    representation = get_representation(settings.get("Representation"), train_dataset, settings)

    if not labelled_urls or len(unique_labels) < 2 or settings.get("QueryStrategy") == "Random Sampling":
        possible_labels = [label.strip() for label in settings["Labels"].split(",")]
        unlabelled_predictions = [choice(possible_labels) for _ in unlabelled_urls]
        test_predictions = [choice(possible_labels) for _ in test_urls]
        send_results({
            "unlabelled": {
                url: prediction
                for url, prediction in zip(unlabelled_urls, unlabelled_predictions)
            },
            "test": {
                url: prediction
                for url, prediction in zip(test_urls, test_predictions)
            },
        })
        return

    model = get_machine_learning_model(settings.get("MachineLearningModel"), len(labels))

    send_stats({
        "progress": "Loaded URLs",
    })

    # Load the content urls in the specified representation (may take a while)
    training_inputs = train_dataset.load_urls(labelled_urls, representation, True)

    send_stats({
        "progress": "Loaded labelled dataset contents",
    })

    training_targets = np.array([unique_labels.index(label) for label in labels])

    model.fit(training_inputs, training_targets)

    send_stats({
        "progress": "Model Fitted",
    })

    unlabelled_inputs = train_dataset.load_urls(unlabelled_urls, representation, False)

    send_stats({
        "progress": "Loaded unlabelled dataset contents",
    })

    if test_urls:
        test_inputs = test_dataset.load_urls(test_urls, representation, False)

        send_stats({
            "progress": "Loaded test dataset contents",
        })

        test_predictions = [unique_labels[index] for index in model.predict(test_inputs)]

        send_stats({
            "progress": "Test Dataset Predicted",
        })
    else:
        test_predictions = []

    if len(unlabelled_inputs):
        unlabelled_predictions = [unique_labels[index] for index in model.predict(unlabelled_inputs)]
    else:
        unlabelled_predictions = []

    send_stats({
        "progress": "Unlabelled Dataset Predicted",
    })

    # print("accuracy:", evaluate_model(settings, model, representation, unique_labels))

    send_results({
        "unlabelled": {
            url: prediction
            for url, prediction in zip(unlabelled_urls, unlabelled_predictions)
        },
        "test": {
            url: prediction
            for url, prediction in zip(test_urls, test_predictions)
        },
    })
