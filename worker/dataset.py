from bs4 import BeautifulSoup
import requests
from abc import ABC, abstractmethod
from io import BytesIO
from PIL import Image, UnidentifiedImageError
from urllib.parse import urljoin
from worker.cache import cached_get_multiple, cached_get
from random import shuffle
from urllib.parse import urlparse, unquote
from pathlib import PurePosixPath
from typing import Union
import json


def get_dataset(url: str, credentials=None) -> "Dataset":
    domain = urlparse(url).netloc

    if domain == "github.com":
        return Github(url, credentials)

    return PageWithLinks(url, credentials)


class Dataset(ABC):
    def __init__(self, url: str, credentials: dict = None):
        self.dataset_url = url
        self._urls = []
        self._targets = []

        if credentials is None:
            self.credentials = {}
        else:
            self.credentials = credentials

    def get_all_urls(self) -> list[str]:
        if not self._urls:
            self._urls = self._get_urls()
            shuffle(self._urls)

        return self._urls

    @abstractmethod
    def _get_urls(self) -> list[str]:
        ...

    @abstractmethod
    def get_target(self, url) -> Union[str, None]:
        ...

    def load_images(self, urls: list[str], settings: dict) -> list[Image]:
        images = []

        scaling_mode = settings.get("ScalingMode", "Center Crop")
        scaling_width = int(settings.get("ScalingWidth", "256"))
        scaling_height = int(settings.get("ScalingHeight", "256"))

        for url, content in zip(urls, self.load_urls(urls)):
            image_data = BytesIO()
            image_data.write(content)
            try:
                image = Image.open(image_data).convert("RGB")
            except UnidentifiedImageError:
                print("Error loading image: ", url)
                continue

            if scaling_mode == "Center Crop":
                x1 = (image.width - scaling_width) // 2
                y1 = (image.height - scaling_height) // 2
                x2 = x1 + scaling_width
                y2 = y1 + scaling_height

                image = image.crop((x1, y1, x2, y2))
            elif scaling_mode == "Stretch to Fit":
                image = image.resize((scaling_width, scaling_height))
            elif scaling_mode == "No Scaling":
                pass
            else:
                raise ValueError(f"Invalid scaling mode '{scaling_mode}'")

            images.append(image)

        return images

    def get_unlabelled_urls(self, label_map: dict[str, str]) -> list[str]:
        return [url for url in self.get_all_urls() if url not in label_map]

    def get_labelled_urls(self, label_map: dict[str, str]) -> list[str]:
        return [url for url in self.get_all_urls() if url in label_map]

    def get_labels(self, label_map: dict[str, str]) -> list[str]:
        return [label_map[url] for url in self.get_all_urls() if url in label_map]

    def load_urls(self, urls: list[str], representation=None, train_representation=False):
        if not urls:
            return []

        if representation is None:
            return cached_get_multiple(urls)
            # rtn = []
            # last_report_time = 0
            # cached_get_multiple(urls)
            # for i, url in enumerate(urls):
            #     if time() > last_report_time + 5:
            #         send_stats({"progress": f"Loading url {i}/{len(urls)}"})
            #         last_report_time = time()
            #     rtn.append(self.load_url(url))
            # return rtn

        if train_representation:
            return representation.train(urls, dataset=self)

        return representation.test(urls, dataset=self)


class PageWithLinks(Dataset):
    def _get_urls(self) -> list[str]:
        response = requests.get(self.dataset_url)
        soup = BeautifulSoup(response.text, features="lxml")

        return [
            urljoin(self.dataset_url + "/", link.get('href'))
            for link in soup.find_all('a')
        ]

    def get_target(self, url: str) -> Union[str, None]:
        return None


class Github(Dataset):
    def __init__(self, url: str, credentials: dict = None):
        super().__init__(url, credentials)

        url_parts = PurePosixPath(unquote(urlparse(self.dataset_url).path)).parts
        self.user = url_parts[1]
        self.repo = url_parts[2]
        self.branch = url_parts[4]
        self.repo_path = "/".join(url_parts[5:])

        self._api_url = f"https://api.github.com/repos/{self.user}/{self.repo}/git/trees/{self.branch}?recursive=1"
        self._content_url = f"https://raw.githubusercontent.com/{self.user}/{self.repo}/refs/heads/{self.branch}/"

    def get_target(self, url: str) -> Union[str, None]:
        path = url.removeprefix(self._content_url + self.repo_path)
        parts = PurePosixPath(unquote(urlparse(path).path)).parts

        if len(parts) > 1:
            return parts[-2]

        return None

    def _get_urls(self) -> list[str]:
        tree = json.loads(cached_get(self._api_url))["tree"]

        return [
            self._content_url + leaf['path']
            for leaf in tree
            if leaf["type"] == "blob" and leaf["path"].startswith(self.repo_path)
        ]
