from worker.dataset import Dataset
import numpy as np
from sklearn.preprocessing import StandardScaler
from skimage.feature import hog
from imgbeddings import imgbeddings


def get_representation(representation_name, dataset, settings):
    match representation_name:
        case "Scaled Flattened Image Data":
            return FlattenedImageData(dataset, settings)
        case "Histogram of Oriented Gradients":
            return HistogramOfOrientedGradients(dataset, settings)
        case "CLIP Embedding":
            return CLIPEmbedding(dataset, settings)


class FlattenedImageData:
    def __init__(self, dataset: Dataset, settings):
        self.scaler = StandardScaler()
        self.dataset = dataset
        self.settings = settings

    def _get_flattened_images(self, urls: list[str], dataset=None):
        if dataset is None:
            dataset = self.dataset

        images = np.stack(dataset.load_images(urls, settings=self.settings))
        return images.reshape((images.shape[0], -1))

    def train(self, urls: list[str], *, dataset=None):
        return self.scaler.fit_transform(self._get_flattened_images(urls))

    def test(self, urls: list[str], *, dataset=None):
        return self.scaler.transform(self._get_flattened_images(urls))


class HistogramOfOrientedGradients:
    def __init__(self, dataset: Dataset, settings):
        self.scaler = StandardScaler()
        self.dataset = dataset
        self.settings = settings

    def _get_hog(self, urls: list[str], dataset=None):
        if dataset is None:
            dataset = self.dataset

        return np.stack([
            hog(
                np.array(image),
                orientations=8,
                pixels_per_cell=(16, 16),
                cells_per_block=(1, 1),
                channel_axis=-1,
            )
            for image in dataset.load_images(urls, settings=self.settings)
        ])

    def train(self, urls: list[str], *, dataset=None):
        return self.scaler.fit_transform(self._get_hog(urls), dataset)

    def test(self, urls: list[str], *, dataset=None):
        return self.scaler.transform(self._get_hog(urls), dataset)


class CLIPEmbedding:
    def __init__(self, dataset: Dataset, settings):
        self.embedding = imgbeddings()
        self.dataset = dataset
        self.settings = settings

    def _run(self, urls: list[str], dataset=None):
        if dataset is None:
            dataset = self.dataset

        return self.embedding.to_embeddings(dataset.load_images(urls, settings=self.settings))

    def train(self, urls: list[str], *, dataset=None):
        return self._run(urls, dataset)

    def test(self, urls: list[str], *, dataset=None):
        return self._run(urls, dataset)
