from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.svm import SVC
from xgboost import XGBClassifier


def get_machine_learning_model(model_name, label_count):
    match model_name:
        case "Random Forest":
            return RandomForestClassifier()
        case "Support Vector Machine":
            return SVC(C=50.0 / label_count, probability=True)
        case "Logistic Regression":
            return LogisticRegression(C=50.0 / label_count, penalty="l1", solver="saga", tol=0.1)
        case "XG Boost":
            return XGBClassifier(tree_method="hist")

    raise ValueError(f"Invalid machine learning model '{model_name}'")
