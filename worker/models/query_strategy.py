from modAL.uncertainty import uncertainty_sampling, entropy_sampling, margin_sampling


def get_query_strategy(query_strategy_name, default=uncertainty_sampling):

    query_strategies = {
        "Uncertainty Sampling": uncertainty_sampling,
        "Entropy Sampling": entropy_sampling,
        "Margin Sampling": margin_sampling,
        "Random Sampling": None,
        None: default
    }

    return query_strategies[query_strategy_name]
