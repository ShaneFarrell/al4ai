from datetime import datetime
from django.shortcuts import resolve_url
from website.settings import WEBSITE_ADDRESS
from website.interface.component_types import (SelectionComponent, TextComponent, NumberComponent, ButtonsComponent,
                                               BaseComponent, TextAreaComponent)


class DataType(SelectionComponent):
    title = "Data Type"
    description = "The type of data your dataset contains."
    options = ["Images"]


class DatasetLocation(TextComponent):
    dependencies = [DataType]

    title = "Dataset Location"
    description = "A url pointing to the index page of your dataset.<br/>" \
                  "This index page should be a html page with a hyperlink to each datum of your dataset."


class TestDatasetLocation(TextComponent):
    dependencies = [DataType]

    title = "Test Dataset Location (optional)"
    description = "A url pointing to the index page of your test dataset.<br/>"


class LabelType(SelectionComponent):
    dependencies = [DataType]

    title = "Label Type"
    description = "The kind of labelling you want your oracle to perform.<br/>" \
                  "(Your oracle is the person labelling your data)"
    # item_url = "https://stackoverflow.com/questions/17711146/how-to-open-link-in-a-new-tab-in-html"

    @property
    def options(self) -> list[str]:
        match self.dependency_values[DataType]:
            case "Images":
                return ["Image Classification"]
            case _:
                return []

    @property
    def item_description(self) -> str:
        match self.value:
            case "Image Classification":
                return "Show the oracle images and ask them to select a label from a list.<br/>" \
                       "You will specify the list of possible labels.<br/><br/>" \
                       "For example, you might show pictures of cats and dogs and have oracles " \
                       "click a button labelled either cat or dog for each image."
            case _:
                return ""


class QueryStrategy(SelectionComponent):
    dependencies = [DataType]
    title = "Query Strategy"
    description = "The strategy to use when selecting which data to show your oracle next."
    extended_description = """This is the core of Active Learning. Each time your oracle labels a batch of your data 
       AL4AI will run the machine learning model you specify (in a later setting). Then the data that the machine 
       learning model is least certain about will be shown to the oracle next. The query strategy is the method used 
       to determine which elements are worth showing next.
       <br/><br/>
       A good strategy will select elements that the model is uncertain about,  but it will also try and make sure 
       not to overly focus on that. It is also important to have examples that span the dataset. Just because the 
       machine learning model is very confused about something, that doesn't mean the thing it's confused about is 
       the only important thing."""

    options = ["Uncertainty Sampling", "Entropy Sampling", "Margin Sampling", "Random Sampling"]
    #  1 - the probability of the highest classification probability.
    @property
    def item_description(self) -> str:
        probability_explainer = """When a machine learning model is run, it assigns a probability to each possible label. For example,
        if you were doing image classification and chose "cat, dog, mouse" as the labels, an image
        classification model may say a certain image has a 60% chance of being a cat, a 25% chance of being a
        dog, and a 15% chance of being a mouse."""

        match self.value:
            case "Uncertainty Sampling":
                return f"""Uncertainty sampling selects data with the lowest classification probability of the most
                likely label.
                <br/><br/>
                {probability_explainer}
                <br/><br/>
                Uncertainty sampling looks at that 60% chance of being a cat and says "ok, so there's 40% uncertainty
                in the most likely label". It then selects the labels in the dataset with the highest uncertainty to
                show to the oracles next.
                <br/><br/>
                This query strategy will prioritise labelling data where the machine learning model can't come up
                with one clear answer about what the label should be. The sampling method does not take into account
                any of the other probabilities. This simplicity is often useful as it keeps the focus squarely on
                producing clear single labels. However, some nuance may be lost in situations where the other
                probabilities do matter. If you don't think it matters what the second or third most likely label
                is, and you only want to focus on the most likely, this may be a useful query strategy.
                <br/><br/>
                If you think that the probabilities of the other labels do matter, read the descriptions of
                Entropy Sampling and Margin Sampling. """
            case "Entropy Sampling":
                return """Entropy sampling selects the data where the classification probabilities represents the
                least amount of information. This takes into account the probabilities of all labels.
                <br/><br/>
                Entropy is an incredibly important concept in information theory and is used very often in machine
                learning. Entropy in this context is a measure of how evenly distributed the probabilities are
                between all the possible labels. The lowest entropy is when a single label is 100% likely, and the
                highest entropy is when each label is equally likely.
                <br/><br/>
                Entropy is a useful metric when the probabilities of all labels matter. Entropy will consider a
                distribution like [60% cat, 39% dog, 1% mouse] less worth labelling than a distribution like
                [60% cat, 20% dog, 20% mouse]. The latter distribution has a more even distribution. The machine
                learning model thinks it is probably a cat, but if it's not then it is very unsure. In the former
                distribution the model is quite sure the answer is dog if it isn't cat.
                <br/><br/>
                In the example above entropy sampling produces the opposite result of Margin Sampling. Margin Sampling
                finds it more concerning that the model has two solid but different ideas about what the label might be.
                Entropy sampling finds it more concerning that if the answer is not cat that the model has no idea what
                the second most likely answer is.
                <br/><br/>
                If you think the model having a good "second best" and "third best" answer is important, entropy
                sampling may be a good metric to use. Having a concrete second and third best answer can mean that
                when taking into account potential prediction errors, you are more likely to get an answer that
                is in some way closer to the correct answer. This makes entropy sampling particularly useful for
                datasets where there is the logical concept of being close to the correct answer even when wrong.
                <br/><br/>
                If you think that focusing so much on the probabilities of the other labels in your dataset might
                be a distraction, read the description of Uncertainty Sampling."""
            case "Margin Sampling":
                return f"""Margin sampling selects data with the lowest difference in classification probabilities of
                the two most likely labels.
                <br/><br/>
                {probability_explainer}
                <br/><br/>
                Margin sampling looks at that 60% chance of being a cat, and at that 25% chance of being a dog and
                says "ok, so the decision margin between the most likely and second most likely label is 35%".
                It then selects the labels in the dataset with the lowest decision margin to show to the oracles next.
                <br/><br/>
                So, this query strategy selects the data where the machine learning model has the hardest time telling
                between the two most likely labels. If it is easy to discount many of the labels in your dataset when
                labelling, but it's hard to decide between a small number of remaining labels, this may be a useful
                query strategy. The strategy will focus on making sure to reduce the ambiguity between the most
                likely option and the second most likely.
                <br/><br/>
                For what is in some ways the opposite of Margin Sampling, read the description of Entropy Sampling.
                If you think that focusing so much on the probabilities of the other labels in your dataset might
                be a distraction, read the description of Uncertainty Sampling."""
            case "Random Sampling":
                return """Random sampling selects data at random to show to the oracles.
                This doesn't use a machine learning model.
                <br/><br/>
                Random sampling is surprisingly effective and does have its advantages. A major advantage of random
                sampling is that it usually produces a fairly even spread of samples from within your dataset. This
                can reduce potential biases in your dataset. Naturally, this misses out on a lot of the advantages of
                active learning, but sometimes the lack of bias is important.
                <br/><br/>
                Random sampling is a useful baseline and truly is not a bad way of selecting data because of the
                statistical neutrality of that selection. However, active learning as a field is focused on producing
                selections that are better than random.
                <br/><br/>
                Random sampling also has the advantage of being extremely quick to run. No machine learning is done
                at all. If you are finding the delay caused by running a machine learning algorithm to be
                cumbersome, random sampling may be a good query strategy."""
            case _:
                return ""


class Representation(SelectionComponent):
    dependencies = [DataType, QueryStrategy]

    title = "Data Representation"
    description = "The representation."

    @property
    def required(self):
        return self.dependency_values[QueryStrategy] != "Random Sampling"

    @property
    def options(self) -> list[str]:
        if not self.required:
            return []

        match self.dependency_values[DataType]:
            case "Images":
                return ["Histogram of Oriented Gradients", "CLIP Embedding", "Scaled Flattened Image Data"]
            case _:
                return []

    @property
    def item_urls(self) -> dict:
        match self.value:
            case "Histogram of Oriented Gradients":
                return {
                    "More Detailed Tutorial": "https://learnopencv.com/histogram-of-oriented-gradients/",
                    "Original Paper": "https://ieeexplore.ieee.org/abstract/document/1467360",
                }
            case "CLIP Embedding":
                return {
                    "Open AI Documentation": "https://openai.com/index/clip/",
                    "Original Paper": "https://arxiv.org/abs/2103.00020",
                }
            case _:
                return {}

    @property
    def item_description(self) -> str:
        match self.value:
            case "Histogram of Oriented Gradients":
                return """Histogram of Oriented Gradients (HOG) is a representation which focuses on the directions
                of the boundaries between coloured areas within the image. It is useful for detecting objects or
                people in photographs.
                <br/><br/>
                HOG breaks the image up into a grid small regions of typically 8x8 pixels. It then looks at how the
                colours in those regions change from left to right and from top to bottom. By looking at how the colours
                change in this way, HOG identifies the boundaries between regions of colour in an image. For example
                a region of brown from the trunk of a tree and a region of green from the grass behind it. Focusing
                on boundaries like this is useful in object detection because often it does not matter what colour an
                object is and it is instead just important that the object is a different colour than the background.
                <br/><br/>
                Once HOG has identified the boundaries in the image, it groups these boundaries by the angle of the
                boundary. So it might say the region of the image contains X amount of boundaries at a 20 degree angle,
                and Y amount of boundaries at a 40 degree angle. By breaking the image into small regions and focusing
                on the angles of boundaries, HOG can identify what side of the boundary of an object each region
                represents. If the colours change such that the boundaries in the region are mostly horizontal, then
                that region probably is showing the top or bottom of the object.
                <br/><br/>
                Reducing the information in the image to only the most relevant details like this can greatly aid
                machine learning models in learning. HOG was originally designed for detecting people in images,
                but it may be useful in other applications where focusing on the angles of boundaries between regions
                of colour has predictive value.
                """
            case "CLIP Embedding":
                return """The CLIP embedding uses a machine learning model trained on images scraped from the internet.
                This makes CLIP useful for image classification problems where a general training on a wide selection
                of images is useful.
                <br/><br/>
                The CLIP model was created by Open AI."""
            case _:
                return ""


class BatchSize(NumberComponent):
    dependencies = [DataType]

    title = "Batch Size"
    description = "The number of items to label at once."
    minimum = 1
    integer_only = True
    default = "3"


class MachineLearningModel(SelectionComponent):
    dependencies = [DataType, Representation, QueryStrategy]
    title = "Machine Learning Model"
    description = "The machine learning model to use."

    @property
    def required(self):
        return self.dependency_values[QueryStrategy] != "Random Sampling"

    @property
    def options(self) -> list[str]:
        if not self.required:
            return []

        match self.dependency_values[DataType]:
            case "Images":
                return ["XG Boost", "Support Vector Machine", "Random Forest"]
            case _:
                return []

    @property
    def item_urls(self) -> dict:
        match self.value:
            case "XG Boost":
                return {
                    "XG Boost Tutorial": "https://medium.com/low-code-for-advanced-data-science/"
                                         "xgboost-explained-a-beginners-guide-095464ad418f",
                    "Original Paper": "https://dl.acm.org/doi/abs/10.1145/2939672.2939785",
                    "Paper Comparing Gradient Boosting Algorithms":
                        "https://link.springer.com/article/10.1007/S10462-020-09896-5",
                }
            case "Support Vector Machine":
                return {}
            case "Random Forest":
                return {}
            case _:
                return {}

    @property
    def item_description(self) -> str:
        match self.value:
            case "XG Boost":
                return """XG Boost is a commonly used model applicable to many types of data. It has the advantage
                of being less black box than many other machine learning models.
                <br/><br/>
                Extreme Gradient Boosting (XG Boost) is a gradient boosted ensemble decision tree based model.
                A decision tree model divides a dataset into smaller and smaller subsets based on yes or no questions
                about the data. An ensemble model is a model composed of multiple sub-models where the results of
                these sub-models are combined to produce the final label (for example by running them in parallel and
                taking the majority answer). Gradient boosting is a way of structuring an ensemble model sequentially
                where each sub-model refines the answer of the previous sub-models. XG Boost uses a sequence of
                decision trees where each decision tree refines the answer given by the previous decision tree.
                <br/><br/>
                You can think of it like this: The first decision tree learns how to play "20 questions" with the
                dataset, trying to narrow down what the label could be. It then provides an answer based on this
                game. XG Boost then looks at how its answers compare to the correct answers and sees where the
                decision tree proved unable to capture the nuances of the dataset adequately. It creates a second
                decision tree model which learns to take a different angle on the problem and specifically provide
                answers to the cases the first tree failed at. The results of these trees together are then
                analysed and XG Boost creates a third decision tree. This continues until either a maximum number of
                trees have been created or too little improvement in results occurs.
                <br/><br/>
                The "Extreme" in XG Boost refers to the specific implementation being highly efficient and thus
                quick to run. This is useful in active learning because the oracle may be waiting for the algorithm
                to finish running so it can serve them new data to label. XG Boost is a good general purpose machine
                learning model and is a reasonable choice for most datasets."""
            case "Support Vector Machine":
                return """A Support Vector Machine transforms the dataset into a high dimensional structure where a
                clean decision boundary in the data can be identified.
                <br/><br/>
                Imagine a piece of paper with red dots drawn on the left side of the sheet, and blue dots drawn on
                the right side of the sheet. If we wanted to create a simple rule to identify if a dot is red or blue,
                we could draw a line down the middle and say that if a dot is on the left side of the line it is red,
                otherwise it is blue. This same logic is how SVMs work. In the simplest case a support vector machine
                will look at the individual variables in your dataset and treat them as coordinates in a high
                dimensional space, and then it will find a decision boundary between them. In more typical use cases of
                SVMs an extra step is included. Often clean decision boundaries can not be found when looking at
                the data directly. To deal with this situation SVMs typically include a kernel function which transforms
                the data into an even higher dimensional form. This transformation is designed so that in this high
                dimensional space most datasets do provide clean boundaries to use for classification.
                <br/><br/>
                Compared with XG Boost, SVMs are more robust to outliers and can perform well in cases where each
                individual datum contains a lot of information. XG Boost is less error prone during setup. When
                using a SVM, the choice of what kernel to use matters quite a lot. In XG Boost there is not such
                decision to make. XG Boost is also better for datasets that contain a lot of data."""
            case "Random Forest":
                return "Random Forest."
            case _:
                return ""


class ProjectManagementButtons(ButtonsComponent):
    buttons = [
        {"title": "Download Labels", "redirect": "Dataset", "require_run_button": True},
        {"title": "Delete Project", "redirect": "Delete", "danger": True},
    ]


class LiveResults(BaseComponent):
    dependencies = [DataType]
    template = "components/unique/live_results.html"
    required = False

    def get_data(self) -> dict:
        data = super().get_data()
        stats: list = self.project.stats

        chart = {}

        for stat in stats:
            labelled_count = stat.get("labelled_count")
            accuracy = stat.get("accuracy")
            if accuracy is not None and labelled_count is not None:
                chart[labelled_count] = max(chart.get(labelled_count, 0), accuracy)

        chart_x = sorted(chart.keys())
        chart_y = [chart[x] for x in chart_x]

        return {
            **data,
            "stats": self.project.latest_stats,
            "chart_x": chart_x,
            "chart_y": chart_y,
        }


class SettingsList(BaseComponent):
    template = "components/unique/settings_list.html"
    required = False

    def get_data(self) -> dict:
        data = super().get_data()

        return {
            **data,
            "stats": {
                "".join(" " + char if char.isupper() else char for char in key): value
                for key, value in self.project.settings.items()
            },
        }


class OracleURLs(BaseComponent):
    template = "components/unique/oracle_url_list.html"
    required = False

    def get_data(self) -> dict:
        data = super().get_data()

        return {
            **data,
            "oracles": {
                oracle.name: WEBSITE_ADDRESS + resolve_url("labelling", oracle.id)
                for oracle in self.project.oracle_set.all()
            },
        }


class Oracles(TextComponent):
    dependencies = [QueryStrategy]

    title = "Oracle Name"
    description = """The name of the person labelling your data (known as the oracle).<br/>
    <br/>
    This will appear alongside the labels when downloading your data.<br/>
    If the person labelling your data changes, you can use this field to keep track of who labelled what.
    <br/><br/>
    If you change the name of your oracle, this will generate a new oracle link. This allows you to invalidate
    links given to oracles that are no longer labelling your data."""
    default = "Oracle"


class Labels(TextComponent):
    dependencies = [QueryStrategy]

    title = "Labels"
    description = "The possible labels an oracle can select. In a comma separated list."
    default = "1, 2, 3, 4, 5, 6, 7, 8, 9, 0"


class Instructions(TextAreaComponent):
    dependencies = [QueryStrategy]

    title = "Instructions"
    description = "The instructions that will be shown to your oracle."


class ScalingMode(SelectionComponent):
    dependencies = [DataType, Representation, QueryStrategy]

    title = "Scaling Mode"
    description = "The scaling mode."

    @property
    def required(self):
        return (self.dependency_values[DataType] == "Images" and
                self.dependency_values[QueryStrategy] != "Random Sampling")

    @property
    def options(self) -> list[str]:
        if not self.required:
            return []

        return ["Center Crop", "Stretch to Fit"]

    @property
    def item_description(self) -> str:
        match self.value:
            case "Center Crop":
                return "Crop the specified size if it is too large. When cropping the image use a center aligned crop."
            case "Stretch to Fit":
                return "Stretch the image to the specified size. This can alter the aspect ratio."
            case _:
                return ""


class ScalingWidth(NumberComponent):
    dependencies = [ScalingMode]

    title = "Scaling Width"
    description = "The width to scale images to."
    minimum = 1
    integer_only = True
    default = "256"


class ScalingHeight(NumberComponent):
    dependencies = [ScalingMode]

    title = "Scaling Height"
    description = "The height to scale images to."
    minimum = 1
    integer_only = True
    default = "256"
