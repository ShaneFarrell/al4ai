from website.models import Project
from abc import ABC, abstractmethod
from typing import Type

all_components: set[Type["BaseComponent"]] = set()


def get_dependencies(component):
    dependencies = set(component.dependencies)
    for dependency in dependencies.copy():
        dependencies |= get_dependencies(dependency)
    return dependencies


class BaseComponent(ABC):
    default: str = ""
    dependencies: list[Type["BaseComponent"]] = []
    template: str
    required: bool = True

    def __init_subclass__(cls):
        if ABC not in cls.__bases__:
            all_components.add(cls)

    @property
    @abstractmethod
    def template(self) -> str:
        ...

    @property
    def enabled(self) -> bool:
        if not all(self.dependency_values.values()):
            return False

        for dependency in self.dependencies:
            if not dependency(self.project).enabled:
                return False

        return True

    def __init__(self, project: Project):
        self.project = project
        self.name = self.__class__.__name__
        self.value: str = self.project.settings.get(self.name, self.default)
        self.dependencies = list(get_dependencies(self))
        self.dependency_values = {}

        for dependency_class in self.dependencies:
            dependency = dependency_class(project)
            self.dependency_values[dependency_class] = dependency.value

    def get_data(self) -> dict:
        return {
            "enabled": self.enabled,
            "template": self.template,
            "name": self.name,
        }

    def submit(self, value: str) -> None:
        pass


class InputComponent(BaseComponent, ABC):
    title: str
    description: str
    auto_refresh: bool

    @property
    @abstractmethod
    def title(self) -> str:
        ...

    @property
    @abstractmethod
    def description(self) -> str:
        ...

    @property
    @abstractmethod
    def auto_refresh(self) -> bool:
        ...

    @property
    def item_description(self) -> str:
        return ""

    @property
    def item_urls(self) -> dict:
        return {}

    @property
    def extended_description(self) -> str:
        return ""

    def get_data(self) -> dict:
        return {
            **super().get_data(),
            "value": self.value,
            "auto_refresh": self.auto_refresh,
            "title": self.title,
            "description": self.description,
            "extended_description": self.extended_description,
            "item_description": self.item_description,
            "item_urls": self.item_urls,
        }

    def submit(self, value: str) -> None:
        if not self.get_data()["enabled"]:
            value = self.default

        self.project.settings = {**self.project.settings, self.name: value}
