from website.interface import components as c
from website.models import Project
from website.interface.component_types import BaseComponent, all_components


page_layouts = {
    "Idle": {
        "Basic Details": [c.DataType, c.LabelType, c.DatasetLocation],
        "Active Learning": [c.QueryStrategy, c.BatchSize, c.Labels],
        "Labelling": [c.Oracles, c.Instructions],
        "Representation": [c.Representation, c.ScalingMode, c.ScalingWidth, c.ScalingHeight],
        "Model": [c.MachineLearningModel],
        "Testing": [c.TestDatasetLocation, c.LiveResults],
        "Overview": [c.ProjectManagementButtons, c.SettingsList],
        "__default__": "Basic Details",
    },
    "Running": {
        "Live Results": [c.LiveResults, c.OracleURLs],
        "Overview": [c.ProjectManagementButtons, c.SettingsList],
        "__default__": "Live Results",
    }
}


def get_default_page(project: Project) -> str:
    return page_layouts[project.state]["__default__"]


def get_available_pages(project: Project) -> list[str]:
    return [
        page
        for page in page_layouts[project.state]
        if page != "__default__"
        if any(
            component(project).get_data()["enabled"]
            for component in page_layouts[project.state][page]
        )
    ]


def get_components(project: Project, page: str) -> list[BaseComponent]:
    return [component(project) for component in page_layouts[project.state][page]]


def components_fully_specified(project: Project) -> bool:
    for component in all_components:
        component = component(project)
        if component.required and not component.value:
            return False

    return True


def page_has_errors(project: Project, page: str) -> bool:
    for component in page_layouts[project.state][page]:
        component = component(project)
        if component.required and not component.value:
            return True

    return False
