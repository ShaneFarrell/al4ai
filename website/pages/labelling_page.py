from django.shortcuts import render, HttpResponse
from website.models import Oracle
from django.utils.timezone import localtime
from datetime import timedelta


def labelling_view(request, oracle_id: str):
    oracle: Oracle = Oracle.objects.filter(id=oracle_id).first()

    if not oracle or not oracle.label_next or not oracle.project.running:
        return render(request, "pages/labelling_pages/idle.html", context={"context": {
            "valid": oracle is not None,
            "project_running": oracle.project.running if oracle else False,
            "oracle_name": oracle.name if oracle else "",
            "project": oracle.project.name if oracle else "",
        }})

    project = oracle.project

    print(oracle.name, oracle.project.name, oracle.label_next)

    options: list[str] = project.settings.get("Labels", "").split(",")
    options = [option.strip() for option in options if option.strip()]

    page = "pages/labelling_pages/images/classification.html"
    context = {
        "project": project.name,
        "oracle": oracle_id,
        "oracle_name": oracle.name,
        "auto_submit": oracle.auto_submit,
        "instructions": project.settings.get("Instructions"),
        "datum": {
            "url": oracle.label_next[0],
            "options": options,
        },
    }

    return render(request, page, {"context": context})


def labelling_ping_view(request, oracle_id: str):
    oracle: Oracle = Oracle.objects.filter(id=oracle_id).first()

    if not oracle or not oracle.project.running:
        return HttpResponse("-1")

    if oracle.label_next:
        return HttpResponse("0")

    time_difference = localtime() - max(oracle.updated, oracle.project.updated)

    if time_difference > timedelta(days=1):
        return HttpResponse("-1")

    if time_difference > timedelta(hours=1):
        return HttpResponse("120")

    if time_difference > timedelta(minutes=15):
        return HttpResponse("60")

    if time_difference > timedelta(minutes=5):
        return HttpResponse("30")

    if time_difference > timedelta(minutes=2):
        return HttpResponse("15")

    if time_difference > timedelta(seconds=30):
        return HttpResponse("5")

    return HttpResponse("1")

