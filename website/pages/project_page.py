from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from website.models import Project
from website.settings import SOURCE_CODE_ADDRESS
from website.interface.page_layouts import (get_available_pages, get_default_page, get_components,
                                            components_fully_specified, page_has_errors)


@login_required
def project_view(request, project_name: str, page: str = "_"):
    project: Project = request.user.get_project(project_name)

    if not project:
        return redirect("home")

    available_pages = get_available_pages(project)

    if page not in available_pages:
        return redirect("project", project_name, get_default_page(project))

    page_components = [component.get_data() for component in get_components(project, page)]

    context = {
        "project": project_name,
        "page": page,
        "header": {
            "project_list": request.user.project_names,
            "show_run_button": components_fully_specified(project) or project.running,
            "running": project.running,
        },
        "sidebar": {
            "pages": available_pages,
            "errors": {page for page in available_pages if page_has_errors(project, page)},
            "show_admin": request.user.is_staff,
            "source_code": SOURCE_CODE_ADDRESS,
        },
        "components": page_components,
    }

    return render(request, "pages/page.html", {"context": context})

