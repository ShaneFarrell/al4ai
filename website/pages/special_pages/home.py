from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required


@login_required
def home_view(request):
    if project := request.user.most_recent_project:
        return redirect("project", project.name, "_")

    return redirect("create")
