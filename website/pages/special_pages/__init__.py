from .authentication import login_view, logout_view
from .create import create_view
from .delete import delete_view
from .home import home_view
