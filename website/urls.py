from django.urls import path
from . import pages, submit, worker
from django.contrib import admin


urlpatterns = [
    path("", pages.home_view, name="home"),
    path("admin/", admin.site.urls),
    path("login/", pages.login_view, name="login"),
    path("projects/", pages.create_view, name="create"),

    path("projects/<project_name>/", pages.project_view),
    path("projects/<project_name>/Delete", pages.delete_view),
    path("projects/<project_name>/Dataset", pages.generate_dataset_view),
    path("projects/<project_name>/Dataset/ping", pages.dataset_ping_view),
    path("projects/<project_name>/Dataset/download/train", pages.download_train_dataset_view),
    path("projects/<project_name>/Dataset/download/test", pages.download_test_dataset_view),
    path("projects/<project_name>/<page>", pages.project_view, name="project"),

    path("labelling/<oracle_id>", pages.labelling_view, name="labelling"),
    path("labelling/<oracle_id>/ping", pages.labelling_ping_view),

    path("submit/", submit.submit_view, name="submit"),
    path("worker/results", worker.worker_results_view),
    path("worker/stats", worker.worker_stats_view),
    path("worker/error", worker.worker_error_view),
]
